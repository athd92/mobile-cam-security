from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDRoundFlatIconButton
from kivy.factory import Factory
from kivy.core.window import Window

Window.size = (400, 600)


class MainApp(MDApp):
    dialog = None

    def build(self):
        self.theme_cls.material_style = "M3"
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "BlueGray"
        return Builder.load_file("alert.kv")

    def show_alert_dialog(self):
        if not self.dialog:
            self.dialog = MDDialog(
                title="Pretty Neat, Right?!",
                text="This is just some text that goes here...",
                buttons=[
                    MDRoundFlatIconButton(
                        text="CLOSE",
                        text_color=self.theme_cls.primary_color,
                        on_release=self.close_dialog,
                        line_color="red",
                    ),
                ],
            )

        self.dialog.open()

    # Click Cancel Button
    def close_dialog(self, obj):
        # Close alert box
        self.dialog.dismiss()

    # Click the Neat Button
    def neat_dialog(self, obj):
        self.dialog.dismiss()
        self.root.ids.my_label.text = "Just a test!"


MainApp().run()
